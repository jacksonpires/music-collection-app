Rails.application.routes.draw do
  resources :albums
  get 'welcome/index'
  devise_for :users
  resources :artist_list, only: %i[index show]

  root to: 'welcome#index'
end
