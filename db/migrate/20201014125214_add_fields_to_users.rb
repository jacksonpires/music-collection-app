class AddFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :full_name, :string, null: false
    add_column :users, :username, :string, null: false, index: true, unique: true
    add_column :users, :role, :integer, null: false, default: 0
  end
end
