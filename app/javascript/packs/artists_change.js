export function artistsChange() {
  const selectedArstist = $('#artists').val();
  window.location.href = `/artist_list/${selectedArstist}`;
}

$(() =>
  $('#artists').on('change', () => artistsChange())
);