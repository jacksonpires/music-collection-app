require 'open-uri'
require 'json'

module Artists
  class RetrieveService
    ENDPOINT = 'https://run.mocky.io/v3/46ca2011-8b43-47cf-b3d7-4df003d48469/'

    def self.get
      json = open(RetrieveService::ENDPOINT).read
      parsed_json = JSON.parse(json)
      selected_options = parsed_json.keys.map { |key| [parsed_json[key]["name"], parsed_json[key]["name"]] }

      selected_options.sort_by { |option| option[0] }
    end
  end
end