require 'open-uri'
require 'json'

class ArtistListController < ProtectedController
  before_action :set_artists, only: [:index, :show]

  def index; end

  def show
    @albums = Album.where(artist: params[:id])
  end

  private

  def set_artists
    @artists = Artists::RetrieveService.get
  end
end
